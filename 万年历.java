  
  
import java.util.Scanner;  
//使用基础的java代码输出日历  
public class Wannianli {  
    public static void main(String[] args) {  
        @SuppressWarnings("resource")
		Scanner s = new Scanner(System.in);  
        System.out.println("请输入年份");  
        int year = s.nextInt();  
        System.out.println("请输入一个月份");  
        int month = s.nextInt();  
        printCalendar(year, month);  
    }  
    /* 
     * 打印日历 
     */  
    private static void printCalendar(int year, int month) {  
        // 打印表头  
        System.out.println("星期日\t星期一\t星期二\t星期三\t星期四\t星期五\t星期六");  
        // 打印数字  
        // 1,空格的数量  
        int kongGe = getKongGeCount(year, month);  
        // 输出空格  
        for (int i = 1; i <= kongGe; i++) {  
            System.out.print("\t");  
        }  
        // 解决当1~7日全为上一月的天数时出现的时间跳转问题  
        if (kongGe == 7) {  
            System.out.println();  
        }  
        // 输出数字  
        for (int i = 1; i <= getDaysOfMonth(year, month); i++) {  
            System.out.print(i + "\t");  
            if ((i + kongGe) % 7 == 0) {  
                System.out.println();  
            }  
        }  
    }  
  
    // 获取空格的数量  
    private static int getKongGeCount(int year, int month) {  
        int count = getDaysOfAll(year, month) % 7 + 1;  
        return count;  
    }  
  
    // 所有的天数  
    private static int getDaysOfAll(int year, int month) {  
        // 整年的天数 + 整月的天数  
        int sum = 0;  
        // 1，整年的天数  
        for (int i = 1900; i < year; i++) {  
            sum += isLoopYear(i) ? 366 : 365;  
        }  
        // 2,整月的天数  
        for (int i = 1; i < month; i++) {  
            sum += getDaysOfMonth(year, i);  
        }  
  
        return sum;  
    }  
  
    /** 
     * 获取整月的天数 
     *  
     * @param year 
     * @param month 
     * @return 
     */  
    private static int getDaysOfMonth(int year, int month) {  
        int day = 0;  
        switch (month) {  
        case 1:  
        case 3:  
        case 5:  
        case 7:  
        case 8:  
        case 10:  
        case 12:  
            day = 31;  
            break;  
        case 4:  
        case 6:  
        case 9:  
        case 11:  
            day = 30;  
            break;  
        case 2:  
            day = isLoopYear(year) ? 29 : 28;  
            break;  
        }  
        return day;  
    }
  
    // 判断是否是闰年  
    private static boolean isLoopYear(int year) {  
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {  
            return true;  
        } else {  
            return false;
        }
    }
}
    
